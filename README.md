# Gastronomia por distritos

A ideia seria um jogo no qual o jogador pudesse associar imagens de comidas típicas de um distrito ao distrito correspondente no mapa de Portugal continental (18 distritos).

Inicialmente o jogador deve escolher entre, por exemplo, pratos de carne, peixe ou doces. 

Depois de escolher a opção pretendida o jogador seria redirecionado para um mapa com os 18 distritos de Portugal continental, onde deveriam aparecer (ao lado do mapa) imagens do prato (carne, peixe ou doces, dependendo da opção escolhida) legendadas com o respetivo nome.

O jogador deverá arrastar a imagem da comida para o distrito que pense ser o correto. No caso de acertar, a imagem preencheria a forma do distrito, somando um ponto à classificação (0-18). No caso de errar, o mapa continuaria inalterado e avançaria para a imagem seguinte. 

Como há vários pratos de cada categoria o ideal seria ter uma base de dados com os vários pratos de carne/peixe/doces para cada distrito e escolher aleatoriamente a imagem apresentada ao jogador.

Nota: Num jogo, só pode aparecer apenas uma imagem para cada distrito (ou seja 18 imagens por jogo) e se, por exemplo, o jogador escolheu pratos de carne, não pode aparecer uma imagem de um doce.

-> Organizar e escolher a informação a utilizar:

	* pratos de cada uma das 3 categorias para cada distrito 
	* quantos pratos de cada categoria (2 ou 3)
	* confirmar se a informação é fidedigna
	* arranjar as imagens corretas para cada prato

-> Como é que se vai detetar se a imagem foi colocada corretamente?

-> O jogo acaba quando tiverem sido mostradas as 18 imagens, aparecendo no final a classificação ( de 0/18 a 18/18).  

-> O que acontece se completar o mapa? Se acertar as 18 imagens, começa a tocar o hino de Portugal. 

-> Aplicação ou web?
